package com.ajc.udemy.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ajc.udemy.model.Invoice;
import com.ajc.udemy.repository.InvoiceRepository;

@Service
public class InvoiceService {
	
	@Autowired
	InvoiceRepository invoiceRepository;
	
	public List<Invoice> findAllByUserId(Long id) {
	    return invoiceRepository.findAllByUserId(id);
	}
	
	

}

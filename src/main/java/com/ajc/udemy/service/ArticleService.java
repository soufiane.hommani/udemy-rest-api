package com.ajc.udemy.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ajc.udemy.model.Article;
import com.ajc.udemy.model.Category;
import com.ajc.udemy.repository.ArticleRepository;

@Service
public class ArticleService {
	
	@Autowired
	ArticleRepository articleRepository;
	
	@Autowired
	CategoryService categoryService;
	
	// READ
		public List<Article> findAll() {
		    return articleRepository.findAll();
		}
		
		public Article setArticleCategory(Article article, Long categoryId) {
		    Optional<Category> categoryOptional = Optional.of(categoryService.findCategoryById(categoryId));
		    if (categoryOptional.isPresent()) {
		        Category category = categoryOptional.get();
		        article.setCategory(category);
		        return articleRepository.save(article);
		    } else {
		        return null;
		    }
		}


		public Optional<Article> findById(Long id) {
			// TODO Auto-generated method stub
			return articleRepository.findById(id);
		}


		public Article save(Article a) {
			// TODO Auto-generated method stub
			return articleRepository.save(a);
		}


		public void deleteById(int id) {
			// TODO Auto-generated method stub
			articleRepository.deleteById(id);
		}


}

package com.ajc.udemy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ajc.udemy.model.OrderLine;
import com.ajc.udemy.repository.OrderLineRepository;

@Service
public class OrderLineService {
  @Autowired
  private OrderLineRepository orderLineRepository;
  
  public OrderLine createOrderLine(OrderLine orderLine) {
    return orderLineRepository.save(orderLine);
  }
}

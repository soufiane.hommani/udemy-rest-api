package com.ajc.udemy.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ajc.udemy.model.Article;
import com.ajc.udemy.model.Cart;
import com.ajc.udemy.model.Invoice;
import com.ajc.udemy.model.Order;
import com.ajc.udemy.model.OrderLine;
import com.ajc.udemy.model.User;
import com.ajc.udemy.repository.InvoiceRepository;
import com.ajc.udemy.repository.OrderLineRepository;
import com.ajc.udemy.repository.OrderRepository;

@Service
public class OrderService {
	
	@Autowired
	private InvoiceRepository invoiceRepository;
	
	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private OrderLineRepository orderLineRepository;

	@Autowired
	private ArticleService articleService;

	@Autowired
	private UserService userService;

	public Order createOrder(Cart cart) {

		Optional<User> user = userService.findById(cart.getUserId());
		if (!user.isPresent()) {
			return null;
		}

		List<Article> articles = new ArrayList<>();
		List<Long> articleIds = cart.getArticlesId();
		for (int i = 0; i < articleIds.size(); i++) {
			Optional<Article> article = articleService.findById(articleIds.get(i));
			if (article.isPresent()) {
				articles.add(article.get());
			}
		}

		List<OrderLine> orderLines = new ArrayList<>();
		for (Article article : articles) {
			OrderLine orderLine = new OrderLine();
			orderLine.setArticle(article);
			orderLine.setPrice(article.getPrice());
			orderLines.add(orderLine);
		}

		Order order = new Order();
		order.setOrderLines(orderLines);
		order.setTotalPrice(calculateTotalPrice(orderLines));
		order.setUser(user.get());

		Order savedOrder = orderRepository.save(order);

		for (OrderLine orderLine : order.getOrderLines()) {
			orderLine.setOrder(savedOrder);
		}

		orderLineRepository.saveAll(savedOrder.getOrderLines());
		

		Invoice invoice = new Invoice(new Date(), savedOrder.getTotalPrice(), user.get(), savedOrder);
		invoiceRepository.save(invoice); 

		return savedOrder;
	}

	private Double calculateTotalPrice(List<OrderLine> orderLines) {
		Double totalPrice = 0.0;
		for (OrderLine orderLine : orderLines) {
			totalPrice += orderLine.getPrice();
		}
		return totalPrice;
	}

	public List<Order> findByUserId(Long userId) {
		User user = new User();
		user.setId(userId);
		return orderRepository.findByUser(user);
	}

	public void createOrderLines(List<OrderLine> orderLines) {
		// TODO Auto-generated method stub

	}
}

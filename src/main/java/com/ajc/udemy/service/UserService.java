package com.ajc.udemy.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ajc.udemy.model.User;
import com.ajc.udemy.repository.UserRepository;

//The service component contains business logic
@Service
public class UserService {
	@Autowired
	UserRepository userRepository;

	// READ
	public List<User> findAll() {
		return userRepository.findAll();
	}

	public Optional<User> findById(Long id) {
		// TODO Auto-generated method stub
		return userRepository.findById(id);
	}

	public User save(User u) {
		// TODO Auto-generated method stub
		return userRepository.save(u);
	}

	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		userRepository.deleteById(id);
	}

	public User authenticate(String username, String password) {
		User optionalUser = userRepository.findByUsernameAndPassword(username, password);
		
		System.out.println("user service "+optionalUser);
		
		if(optionalUser == null){
			return null;
		}
		return optionalUser;

	}

}

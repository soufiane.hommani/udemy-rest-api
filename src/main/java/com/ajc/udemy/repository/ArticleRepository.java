package com.ajc.udemy.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ajc.udemy.model.Article;
import com.ajc.udemy.model.User;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Integer >{
	List<Article> findAll();
	Optional<Article> findById(Long id);
	Optional<Article>  findByName(String Articlename);
}

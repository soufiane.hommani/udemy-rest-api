package com.ajc.udemy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ajc.udemy.model.Article;
import com.ajc.udemy.model.Order;
import com.ajc.udemy.model.OrderLine;

@Repository
public interface OrderLineRepository extends JpaRepository<OrderLine, Long> {
	List<OrderLine> findByOrder(Order order);

	List<OrderLine> findByArticle(Article article);
}

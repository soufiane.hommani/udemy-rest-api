package com.ajc.udemy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ajc.udemy.model.Order;
import com.ajc.udemy.model.User;

@Repository
public interface OrderRepository  extends JpaRepository<Order, Integer>{
	
	
	//Récupération des commandes d'un utilisateur donné
	List<Order> findByUser(User user);

	List<Order> findLastByUserId(int id);
	
	
	
	
	
}

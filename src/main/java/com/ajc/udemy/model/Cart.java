package com.ajc.udemy.model;

import java.util.List;

public class Cart {

	private Long id;
	private Long userId;
	private List<Long> articlesId;

	@Override
	public String toString() {
		return "Cart [id=" + id + ", userId=" + userId + ", articlesId=" + articlesId + "]";
	}

	public Cart() {
	}

	public Cart(Long id, Long userId, List<Long> articlesId) {
		this.id = id;
		this.userId = userId;
		this.articlesId = articlesId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public List<Long> getArticlesId() {
		return articlesId;
	}

	public void setArticlesId(List<Long> articlesId) {
		this.articlesId = articlesId;
	}

}

package com.ajc.udemy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ajc.udemy.model.Cart;
import com.ajc.udemy.model.Order;
import com.ajc.udemy.model.OrderLine;
import com.ajc.udemy.service.ArticleService;
import com.ajc.udemy.service.OrderService;
import com.ajc.udemy.service.UserService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/orders")
public class OrderController {
	@Autowired
	private OrderService orderService;

	@Autowired
	private ArticleService articleService;

	@Autowired
	private UserService userService;
	
	

	@PostMapping("")
	public Order createOrder(@RequestBody Cart cart) {
		
		System.out.println("hello");

		Order savedOrder = orderService.createOrder(cart);
		
		
		
		System.out.println("hello");
		
		Order order = new Order();
		
		//order.setUser(savedOrder.getUser());
		order.setId(savedOrder.getId());
	order.setTotalPrice(savedOrder.getTotalPrice());
		order.setOrderLines(savedOrder.getOrderLines());


		
		
		return order;
	}
	// passer un panier ac slment les id d'articles en @RequestBody
	// ne pas se fier au prix récupérer du front

	// pour l'entité Panier ne pas mettre @Entity
	// boucler et recp les articles + calculer le prix total ds le service
	// instancier des lignes de commandes a partir de ces article
	// calculer le total de la commande
	// instancier un Order ac ces orderlines

	// ds le body :
	/*
	 * { "user": { "id": 1 }, "cart": [ { "article": { "id": 1 } }, { "article":
	 * { "id": 2 } } ] }
	 */

	// ou juste tableau cart:[id_1, id_2 etc]

	/*
	 * for (OrderLine orderLine : order.getOrderLines()) {
	 * orderLine.setOrder(order); }
	 * 
	 * 
	 * Order savedOrder = orderService.createOrder(order); URI location =
	 * ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").
	 * buildAndExpand(savedOrder.getId()) .toUri();
	 */
	// return ResponseEntity.created(location).build();

}
package com.ajc.udemy.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ajc.udemy.model.Article;
import com.ajc.udemy.model.Category;
import com.ajc.udemy.model.Order;
import com.ajc.udemy.model.OrderLine;
import com.ajc.udemy.repository.CategoryRepository;
import com.ajc.udemy.service.ArticleService;
import com.ajc.udemy.service.CategoryService;
import com.ajc.udemy.service.OrderService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/article")
public class ArticleController {

	@Autowired
	ArticleService articleService;
	
	@Autowired
    private CategoryService categoryService;

	@Autowired
	private OrderService orderService;

	@GetMapping("")
	public List<Article> findall() {
		
		/*
		 * Category webCategory = categoryService.findCategoryById(1L);
		 * System.out.println("webCategory : "+webCategory); List<Article> articleList =
		 * articleService.findAll(); for(Article art: articleList) {
		 * articleService.setArticleCategory(art, webCategory.getId());
		 * System.out.println("art : " + art); }
		 */
		
		return  articleService.findAll();
	}
	
	 @GetMapping("/byuser/{userId}")
	  public List<Article> getArticlesByUser(@PathVariable Long userId) {
	    // Get the orders for the user
	    List<Order> orders = orderService.findByUserId(userId);
	    
	    // Extract the order lines from the orders
	    List<OrderLine> orderLines = new ArrayList<>();
	    for (Order order : orders) {
	      orderLines.addAll(order.getOrderLines());
	    }
	    
	    // Extract the articles from the order lines
	    List<Article> articles = new ArrayList<>();
	    for (OrderLine orderLine : orderLines) {
	      articles.add(orderLine.getArticle());
	    }
	    
	    System.out.println(articles);
	    
	    return articles;
	  }

	@GetMapping("/{id}")
	public Article findbyid(@PathVariable(name = "id") Long id) {
		return articleService.findById(id).get();
	}

	@PostMapping("")
	public Article createArticle(@RequestBody Article article) {

		System.out.println("article" + article);

		return articleService.save(article);
	}

	@CrossOrigin
	@PutMapping("") // update
	public Article update(@RequestBody Article article) {
		System.out.println("article update" + article);
		return articleService.save(article);
	}

}

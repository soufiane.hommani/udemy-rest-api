package com.ajc.udemy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ajc.udemy.model.Article;
import com.ajc.udemy.model.Invoice;
import com.ajc.udemy.repository.InvoiceRepository;
import com.ajc.udemy.service.InvoiceService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/invoice")
public class InvoiceController {

	
	@Autowired
	InvoiceService invoiceService;
	
	@GetMapping("/{userId}")
	public List<Invoice> findallByUserId(@PathVariable(name = "userId") Long id) {
		System.out.println("id " + id );
		return  invoiceService.findAllByUserId(id);
	}
	
	
}

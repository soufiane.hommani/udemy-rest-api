package com.ajc.udemy.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ajc.udemy.model.User;
import com.ajc.udemy.service.UserService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserService userService;

	@GetMapping("")
	public List<User> findall() {
		return userService.findAll();
	}

	@GetMapping("/{id}")
	public User findbyid(@PathVariable(name = "id") Long id) {

		User u = userService.findById(id).get();

		System.out.println("user found with id" + u);

		User foundUser = new User();

		foundUser.setId(u.getId());
		foundUser.setFirstname(u.getFirstname());
		foundUser.setLastname(u.getLastname());
		foundUser.setAdress(u.getAdress());
		foundUser.setPassword(u.getPassword());
		foundUser.setUsername(u.getUsername());

		return foundUser;
	}

	@PutMapping("") // update
	public User update(@RequestBody User updatedUser) {

		System.out.println("user u" + updatedUser);

		User u = userService.save(updatedUser);

		User foundUser = new User();

		foundUser.setId(u.getId());
		foundUser.setFirstname(u.getFirstname());
		foundUser.setLastname(u.getLastname());
		foundUser.setAdress(u.getAdress());
		foundUser.setPassword(u.getPassword());
		foundUser.setUsername(u.getUsername());

		return foundUser;
	}

	@DeleteMapping("/{id}")
	public String delete(@PathVariable(name = "id") Long id) {

		userService.deleteById(id);
		return "personne supprim�e";
	}

	public boolean isUserAlreadyCreated(User u) {

		if (u == null) {
			return false;
		}

		List<User> users = this.findall();

		for (int i = 0; i < users.size(); i++) {
			User user = users.get(i);

			if (user.getFirstname() == null || user.getLastname() == null) {
				continue;
			}

			if (user.getUsername().equals(u.getUsername()) && user.getPassword().equals(u.getPassword())) {
				return true;
			}
		}
		return false;
	}

	@PostMapping("/signin")
	public User authenticate(@RequestBody String[] payload) {

		System.out.println();

		String username = payload[0];
		String password = payload[1];

		System.out.println(username + "  " + password);

		User u = userService.authenticate(username, password);

		System.out.println("user found: " + u);

		if (u == null) {
			return null;
		}

		User foundUser = new User();

		foundUser.setId(u.getId());
		foundUser.setFirstname(u.getFirstname());
		foundUser.setLastname(u.getLastname());
		foundUser.setAdress(u.getAdress());
		foundUser.setPassword(u.getPassword());
		foundUser.setUsername(u.getUsername());

		System.out.println("foundUser" + foundUser);

		return foundUser;
	}

	@PostMapping("")
	public User create(@RequestBody User u) {

		System.out.println("utttt");
		if (u == null) {

		}

		if (this.isUserAlreadyCreated(u)) {
			System.out.println("passe ici");
			return null;
		}

		System.out.println("passe ici ausiis");

		User createdUser = userService.save(u);
		
		
		System.out.println("createdUser " + createdUser);
		
		return createdUser;
	}

}

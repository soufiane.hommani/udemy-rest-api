package com.ajc.udemy.annotation;

public class JsonViews {

	public static class Common {

	}

	public static class orderWithOrderLine extends Common {

	}

	public static class OrderLineWithOrder extends Common {

	}

	public static class UserWithOrder extends Common {

	}

	public static class OrderWithUser extends Common {

	}
	
	public static class OrderLineWithArticle extends Common {

	}
	
	public static class ArticleWithOrderLine extends Common {

	}

}
